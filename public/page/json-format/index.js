new Vue({
    el: '#app',
    data: {
      form:{
        formOld:''
      },
      formatFlag:true
    },
    computed:{
      formNew(){
        let data = this.form.formOld;
        try {
          let key1 = data.indexOf('{') < 0 ? data.length : data.indexOf('{');
          let key2 = data.lastIndexOf('}');
          let key3 = data.indexOf('[') < 0 ? data.length : data.indexOf('[');
          let key4 = data.lastIndexOf(']');

          if(key1 < key3 && key2 > key4){
            // json object
            data = data.slice(key1,key2+1);
          }else if(key3 < key1 && key4 > key2){
            // json array
            data = data.slice(key3,key4+1);
          }else {
            throw new Error('格式化失败！格式不正确');
          }
          data = JSON.parse(data);
          this.formatFlag = true;
        } catch (error) {
          this.formatFlag = false;
          data = error.message;
        }
        return data;
      }
    }
});
