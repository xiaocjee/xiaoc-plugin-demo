const testMode = false; // 为true时可以在浏览器打开不报错
// vscode webview 网页和普通网页的唯一区别：多了一个acquireVsCodeApi方法
const vscode = testMode ? {} : acquireVsCodeApi();
const callbacks = {};

/**
 * 调用vscode原生api
 * @param data 可以是类似 {cmd: 'xxx', param1: 'xxx'}，也可以直接是 cmd 字符串
 * @param cb 可选的回调函数
 */
function callVscode(data, cb) {
  if (typeof data === "string") {
    data = { cmd: data };
  }
  if (cb) {
    // 时间戳加上5位随机数
    const cbid = Date.now() + "" + Math.round(Math.random() * 100000);
    callbacks[cbid] = cb;
    data.cbid = cbid;
  }
  vscode.postMessage(data);
}

window.addEventListener("message", (event) => {
  const message = event.data;
  switch (message.cmd) {
    case "vscodeCallback":
      console.log(message.data);
      (callbacks[message.cbid] || function () {})(message.data);
      delete callbacks[message.cbid];
      break;
    default:
      break;
  }
});

new Vue({
  el: "#app",
  data: {
    projectName: "加载中",
    form: {
      formUrl: "",
      formMethod: "GET",
      formApplication: "application/x-www-form-urlencoded;charset=UTF-8",
    },
    axiosRequest: {
      code: 200,
      data: [],
    },
    lineObj: {
      formParamsList: [],
      formTokenList: [],
    },
    paramsType: "",
    paramsJson: "",
    axiosParams: {},
    showParamsJsonError: false,
    errorMsg: "",
  },
  mounted() {
    callVscode(
      "getProjectName",
      (projectName) => (this.projectName = projectName)
    );
  },
  methods: {
    // 模拟alert
    alert(info) {
      callVscode({ cmd: "alert", info: info }, null);
    },
    // 弹出错误提示
    error(info) {
      callVscode({ cmd: "error", info: info }, null);
    },
    openFileInVscode() {
      callVscode({ cmd: "openFileInVscode", path: `package.json` }, () => {
        this.alert("打开package.json成功！");
      });
    },

    getToken() {
      let tokenObj = {};
      let obj = this.lineObj.formTokenList
        .filter((item) => item.key !== "")
        .forEach((item) => {
          tokenObj[item.key] = item.value;
        });
      return tokenObj;
    },
    isEmpty(val) {
      if (val === undefined || val === null || val === "" || val.length === 0) {
        return true;
      }
      if (typeof val === "string") {
        if (val.trim().length === 0) {
          return true;
        }
      } else if (typeof val === "object") {
        if (JSON.stringify(val) === "{}") {
          return true;
        }
      }
      return false;
    },
    isJSON(str) {
      let data = typeof str !== "string" ? JSON.stringify(str) : str;
      this.errorMsg = "";
      try {
        var obj = JSON.parse(data);
        if (
          typeof obj === "object" &&
          Object.prototype.toString.call(obj) === "[object Object]"
        ) {
          return true;
        } else {
          return false;
        }
      } catch (e) {
        this.errorMsg = e.message;
        return false;
      }
    },
    formatParams(obj = {}) {
      let par = "";
      for (const key in obj) {
        par += `&${key}=${escape(obj[key])}`;
      }
      par = par.length ? "?" + par.substring(1) : "";
      return par;
    },
    getParams() {
      const { formMethod } = this.form;
      let paramsObj = {};
      let paramsArr = ["params", {}];

      if (this.paramsType === "list") {
        this.lineObj.formParamsList
          .filter((item) => item.key !== "")
          .forEach((item) => {
            paramsObj[item.key] = item.value;
          });
      } else if (
        this.paramsType === "json" &&
        this.paramsJson &&
        this.isJSON(this.paramsJson)
      ) {
        paramsObj = JSON.parse(this.paramsJson);
      } else {
      }

      if (formMethod === "GET") {
        // paramsArr[1] = this.formatParams(paramsObj);
        paramsArr[1] = paramsObj;
      } else if (formMethod === "POST") {
        paramsArr[0] = "data";
        paramsArr[1] = paramsObj;
      }
      return paramsArr;
    },
    addLine(target) {
      this.lineObj[target].push({
        key: "",
        value: "",
      });
    },
    delLine(target, index) {
      this.lineObj[target].splice(index, 1);
    },
    doneAxios() {
      const { formUrl, formMethod, formApplication } = this.form;

      // 设置token
      const headerToken = this.getToken();
      // eslint-disable-next-line @typescript-eslint/naming-convention
      const headers = { "Content-Type": formApplication, ...headerToken };

      const params = {
        method: formMethod,
        url: formUrl,
        headers: { ...headers },
      };

      // 设置参数
      const paramsList = this.getParams();
      params[paramsList[0]] = paramsList[1];

      this.axiosParams = params;

      axios(params)
        .then((res) => {
          this.axiosRequest = res.data;
        })
        .catch((error) => {
          if (error.response) {
            // 请求已发出，但服务器响应的状态码不在 2xx 范围内
            // this.error('请求出错：'+ JSON.stringify(error.response));
            this.axiosRequest = error.response;
          } else {
            // Something happened in setting up the request that triggered an Error
            // this.error('请求出错：'+ JSON.parse(error.message));
            this.axiosRequest = error.message;
          }
        });
    },
  },
  watch: {
    paramsJson(val) {
      console.log("...paramsJson...", val);
      if (val === "") {
        this.showParamsJsonError = false;
      } else {
        this.showParamsJsonError = !this.isJSON(val);
      }
    },
    "form.formMethod": function (val) {
      if (val === "GET" && this.paramsType === "json") {
        this.paramsType = "";
        this.paramsJson = "";
      }
    },
  },
});
