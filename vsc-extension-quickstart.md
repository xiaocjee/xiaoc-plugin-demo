# xiaoc-vscode README

## setting

```json
"xiaoc-vscode.atomicCssPath": {
	"type": "string",
	"default": "/src/common/style/index.scss",
	"description": "原子CSS（Atomic CSS）文件路径"
},
"xiaoc-vscode.scssVariablePath": {
	"type": "string",
	"default": "/node_modules/npm-cts-ui/dist/style/pages/variables.scss",
	"description": "scss变量文件路径(多个路径以逗号[,]分隔)"
}
```

## guide

### xiaoc-vscode.atomicCssPath

> 配置基础 css 类名文件路径，在 tsx 文件中给元素绑定类名是会读取路径文件中的类名作为代码提示显示

![css类名文件](https://gitee.com/xiaocjee/xiaoc-plugin-demo/raw/master/src/images/scss1.png)

![代码提示显示](https://gitee.com/xiaocjee/xiaoc-plugin-demo/raw/master/src/images/scss2.png)

### xiaoc-vscode.scssVariablePath

> 配置 scss 变量文件路径，在 scss 文件中使用时会读取路径文件中的变量作为代码提示和鼠标悬浮提示

![变量文件1](https://gitee.com/xiaocjee/xiaoc-plugin-demo/raw/master/src/images/variable1.png)

![变量文件1](https://gitee.com/xiaocjee/xiaoc-plugin-demo/raw/master/src/images/variable2.png)

![变量代码提示](https://gitee.com/xiaocjee/xiaoc-plugin-demo/raw/master/src/images/variable3.png)

![变量鼠标悬浮提示](https://gitee.com/xiaocjee/xiaoc-plugin-demo/raw/master/src/images/variable4.png)

## other

### 使用@引入文件触发提示

```js
import xx from '@/xxx'

// or

const { xx } = require('@/xx')

// 以上两种引入方式将触发文件提示
// @默认配置为项目路径下的src
```

### 获取当前文件路径

```js
// 插件激活后可以点击右键

> 获取当前文件(夹)路径

// 点击后vscode将提示鼠标所指向的文件的绝对路径
```

:yum:
:thumbsup:
**Enjoy!**
