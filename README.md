# xiaoc-vscode

## 近期更新（Recent Update）

### v0.0.31

remark: 修改接口测试页面参数类型，添加json类型

### v0.0.30

remark: 添加代码片段

| 文件类型      | 触发条件 | 代码片段说明     |
| :---        |    :----:   |          ---: |
| `vue`      | `v2create`       | 生成 vue2 vue文件基础模板   |
| `javascrpt`   | `v2createjs`        |   生成 vue2 js文件基础模板    |
| `javascrpt, typescript, vue`   | `clg`        |   console.log()    |
| `javascrpt, typescript, vue`   | `cle`        |   console.error()    |

### v0.0.17

remark: 添加 JSON 格式化

![接口测试页](https://gitee.com/xiaocjee/xiaoc-plugin-demo/raw/master/src/images/log/log02.png)

### v0.0.16

remark: 添加接口测试页面

![接口测试页](https://gitee.com/xiaocjee/xiaoc-plugin-demo/raw/master/src/images/log/log01.png)

## 功能列表（Feature List）

1. 配置基础 css 类名文件路径，在 tsx 文件中给元素绑定类名是会读取路径文件中的类名作为代码提示显示
1. 配置 scss 变量文件路径，在 scss 文件中使用时会读取路径文件中的变量作为代码提示和鼠标悬浮提示
1. 使用@引入文件触发提示
1. 获取当前文件路径
1. 添加接口测试页面
1. 添加 JSON 格式化

更多功能说明请查看 [change log](https://gitee.com/xiaocjee/xiaoc-plugin-demo/blob/master/CHANGELOG.md)

:yum:
:thumbsup:
**Enjoy!**
