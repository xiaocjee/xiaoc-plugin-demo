// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('恭喜，您的扩展 xiaoc-vscode 已被激活！');
	require('./main/tools/index')(context); // 工具命令
	require('./main/completion/index')(context); // 自动补全
	require('./main/code-hinting/index')(context); // 代码提示
	require('./main/hover-tip/index')(context); // 悬浮提示
	require('./main/webview/index')(context); // 接口测试页面
}

// this method is called when your extension is deactivated
export function deactivate() {
	console.log('您的扩展已 xiaoc-vscode 被释放！');
}
