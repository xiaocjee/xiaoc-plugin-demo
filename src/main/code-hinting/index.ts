import * as vscode from 'vscode';

import showScssVariableCode from './show-scss-variable-code';

module.exports = function(context:vscode.ExtensionContext) {
  // 注册代码建议提示，只有当按下“.”时才触发
	context.subscriptions.push(vscode.languages.registerDefinitionProvider(['scss'], {
		provideDefinition:showScssVariableCode
	}));
};
