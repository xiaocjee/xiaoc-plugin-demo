import * as vscode from 'vscode';
import * as fs from "fs";

import {getProjectSrcPath,isFileExist} from '../../lib/util';
const scssVariablePath:string = vscode.workspace.getConfiguration().get('xiaoc-vscode.scssVariablePath') || '';

// ==============================  scss变量文件提示 =====================================

/**
 * 提示scss全局变量
 * @param document 文档对象
 * @param position 当前行与列
 * @param token -
 */
export default function showScssVariableCode(document:vscode.TextDocument, position:vscode.Position, token:vscode.CancellationToken) {
	const fileName	= document.fileName;
	// 只截取到光标位置为止，防止一些特殊情况
	const word		= document.getText(document.getWordRangeAtPosition(position));
	const line		= document.lineAt(position);
	const projectPath = getProjectSrcPath(document);
	let pathList = scssVariablePath.split(',').filter(item => isFileExist(`${projectPath}${item}`));
	console.log('....2',pathList.length === 0 || word.indexOf("$") === -1);
	if(pathList.length === 0 || word.indexOf("$") === -1){
		return ;
	}
	let destPath = '';
	let data = '';
	console.log('...1');
	
	for (const item of pathList) {
		destPath = `${projectPath}${item}`;
		data = fs.readFileSync(destPath).toString('utf8').trim();
		if(data.indexOf(word) !== -1){
			return new vscode.Location(vscode.Uri.file(destPath), new vscode.Position(0, 0));
		}
	}
}