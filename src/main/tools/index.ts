import * as vscode from 'vscode';
module.exports = function(context: vscode.ExtensionContext) {
  
	/** 获取当前文件路径 */
	context.subscriptions.push(vscode.commands.registerCommand('xiaoc-vscode.getCurrentFilePath', (uri) => {
		vscode.window.showInformationMessage(`当前文件(夹)路径是：${uri ? uri.path : '空'}`);
	}));
};