import * as vscode from 'vscode';
import {getProjectPath,getWebViewContent} from '../../lib/util';

export default function(context:vscode.ExtensionContext) {
  // 注册命令，可以给命令配置快捷键或者右键菜单
  // 回调函数参数uri：当通过资源管理器右键执行命令时会自动把所选资源URI带过来，当通过编辑器中菜单执行命令时，会将当前打开的文档URI传过来
  return vscode.commands.registerCommand('xiaoc-vscode.jsonFormat', function (uri) {
    const projectPath = getProjectPath(uri);
    if (!projectPath) return;
    // 创建webview
    const panel = vscode.window.createWebviewPanel(
      'jsonWebview', // viewType
      "JSON 格式化", // 视图标题
      vscode.ViewColumn.One, // 显示在编辑器的哪个部位
      {
        enableScripts: true, // 启用JS，默认禁用
        retainContextWhenHidden: true, // webview被隐藏时保持状态，避免被重置
      }
    );
    panel.webview.html = getWebViewContent(context, 'public/page/json-format/index.html');
  })
};
