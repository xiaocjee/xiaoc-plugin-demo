import * as vscode from 'vscode';
import {showInfo,showError,getProjectName,openFileInVscode,getProjectPath,getWebViewContent,invokeCallback} from '../../lib/util';

interface msgtype {
	cmd: 'alert' | 'error' | 'getProjectName' | 'openFileInVscode'
	[prop:string] :any
}

/**
 * 存放所有消息回调函数，根据 message.cmd 来决定调用哪个方法
 */
 const messageHandler = {
	// 弹出提示
	alert(global:any, message:any) {
			showInfo(message.info);
	},
	// 显示错误提示
	error(global:any, message:any) {
			showError(message.info);
	},
	// 获取工程名
	getProjectName(global:any, message:any) {
			invokeCallback(global.panel, message, getProjectName(global.projectPath));
	},
	openFileInVscode(global:any, message:any) {
			openFileInVscode(`${global.projectPath}/${message.path}`, message.text);
			invokeCallback(global.panel, message, {code: 0, text: '成功'});
	},
};

export default function(context:vscode.ExtensionContext) {
    // 注册命令，可以给命令配置快捷键或者右键菜单
    // 回调函数参数uri：当通过资源管理器右键执行命令时会自动把所选资源URI带过来，当通过编辑器中菜单执行命令时，会将当前打开的文档URI传过来
		return vscode.commands.registerCommand('xiaoc-vscode.openWebview', function (uri) {
			const projectPath = getProjectPath(uri);
			if (!projectPath) return;

			// 创建webview
			const panel = vscode.window.createWebviewPanel(
				'axiosWebview', // viewType
				"接口测试", // 视图标题
				vscode.ViewColumn.One, // 显示在编辑器的哪个部位
				{
					enableScripts: true, // 启用JS，默认禁用
					retainContextWhenHidden: true // webview被隐藏时保持状态，避免被重置
				}
			);
			let global = { projectPath, panel};
			panel.webview.html = getWebViewContent(context, 'public/page/interface-testing/index.html');
			panel.webview.onDidReceiveMessage((message : msgtype) => {
				console.log('message',message);
					if (messageHandler[message.cmd]) {
							messageHandler[message.cmd](global, message);
					} else {
							showError(`未找到名为 ${message.cmd} 回调方法!`);
					}
			}, undefined, context.subscriptions)
		})
};
