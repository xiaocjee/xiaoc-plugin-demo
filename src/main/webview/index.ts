import * as vscode from 'vscode';
import interfaceTesting from './interface-testing'
import jsonFormat from './json-format'

module.exports = function(context:vscode.ExtensionContext) {

  // 接口测试页
  const myInterfaceTesting = interfaceTesting(context)
  context.subscriptions.push(myInterfaceTesting);

  // json格式化
  const myJsonFormat = jsonFormat(context)
  context.subscriptions.push(myJsonFormat);
};
