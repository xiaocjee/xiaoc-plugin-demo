import * as vscode from 'vscode';
import * as fs from "fs";
import {getProjectSrcPath,isFileExist} from '../../lib/util';
const atomicCssPath = vscode.workspace.getConfiguration().get('xiaoc-vscode.atomicCssPath');


// ==============================  原子css提示 =====================================
/**
 * 判断是否显示原子css类名提示
 * @param str 当前行代码数据
 */
 function checkAtomicClassName(str:string):boolean{
	let num1 = str.indexOf('class=');
	let str2 = str.slice(num1+6);
	let flag = false;

	if(str2.charAt(0) === '\''){
		if(str2.slice(1).indexOf('\'')===-1){
			flag = true;
		}
	}else if(str2.charAt(0) === '"'){
		if(str2.slice(1).indexOf('"')===-1){
			flag = true;
		}
	}else if(str2.charAt(0) === '`'){
		if(str2.slice(1).indexOf('`')===-1){
			flag = true;
		}
	}
	return flag;
}

/**
 * 提取原子css文件内容，作为className智能提示
 * @param document 文档对象
 * @param position 当前行与列
 * @param token -
 * @param context 上下文对象
 */
export default function showAtomicCssClassVue(document:any, position:any, token:any, context:any) {
	const line        = document.lineAt(position);
	const projectPath = getProjectSrcPath(document);

	const lineText = line.text.substring(0, position.character);
	let str = lineText.toString().trimRight();
	let flag = checkAtomicClassName(str);

	if(flag && isFileExist(`${projectPath}${atomicCssPath}`)) {
		let list:string[] = [];
		let files = fs.readFileSync(`${projectPath}${atomicCssPath}`);
		let data  = files.toString('utf8');
		const dependencies = data.match(/\..*\{/g) || [];
		dependencies.forEach(dep => {
			let val = dep.slice(1,-1).trim();
			if(!list.includes(val)){
				list.push(val);
			}
		});
		return list.map(item => new vscode.CompletionItem(item, vscode.CompletionItemKind.Field));
	}
}