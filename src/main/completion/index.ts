import * as vscode from 'vscode';

import showAtomicCssClass from './show-atomic-css-class';
import showAtomicCssClassVue from './show-atomic-css-class-vue';
import showScssVariable from './show-scss-variable';
import showSrcFile from './show-src-file';


/**
 * 光标选中当前自动补全item时触发动作，一般情况下无需处理
 * @param {*} item
 * @param {*} token
 */
function resolveCompletionItem(item:any, token:any) {
    return null;
}

module.exports = function(context:vscode.ExtensionContext) {
	// 注册代码建议提示

	// scss
	let scssFile = vscode.languages.registerCompletionItemProvider(
		{
			scheme: 'file',
			language: 'scss'
		},
		{
			provideCompletionItems:showScssVariable,
			resolveCompletionItem //光标选中当前自动补全item时触发动作，一般情况下无需处理
		},
		"$",
	);
	context.subscriptions.push(scssFile);

	const TYPES = [
		'typescript',
		'javascript',
		'typescriptreact',
		'javascriptreact',
		'vue',
		'scss'
	];
	// 遍历注册插件需要执行的文本类型
	TYPES.forEach(item => {
			if(item === 'vue'){
				// vue
				let showAtomicCssClassDisposableVue = vscode.languages.registerCompletionItemProvider(
					{
						scheme: 'file',
						language: 'vue'
					},
					{
						provideCompletionItems:showAtomicCssClassVue,
						resolveCompletionItem //光标选中当前自动补全item时触发动作，一般情况下无需处理
					},
					" ",
					"'",
					'"',
				);
				context.subscriptions.push(showAtomicCssClassDisposableVue);
			}else {
				let showAtomicCssClassDisposable = vscode.languages.registerCompletionItemProvider(
					{
						scheme: 'file',
						language: item
					},
					{
						provideCompletionItems:showAtomicCssClass,
						resolveCompletionItem //光标选中当前自动补全item时触发动作，一般情况下无需处理
					},
					"'",
					'"',
					' ',
				);
				context.subscriptions.push(showAtomicCssClassDisposable); // 完成订阅
			}

		let showSrcFileDisposable = vscode.languages.registerCompletionItemProvider(
			{
				scheme: 'file',
				language: item
			},
			{
				provideCompletionItems:showSrcFile,
				resolveCompletionItem //光标选中当前自动补全item时触发动作，一般情况下无需处理
			},
			'/',
		);
		context.subscriptions.push(showSrcFileDisposable); // 完成订阅
	});
};
