import * as vscode from 'vscode';
import * as fs from "fs";
import {getProjectSrcPath,getFileStats} from '../../lib/util';

// ==============================  文件别名@匹配路径下内容 =====================================
/**
 * 检查路径是否正确
 * @param str 文件路径
 */
 function checkSrcFile(str:string):boolean{
	let flag = false,isDir = false;
	let reg = /.*(require\(|from).*@\//g;
	if(reg.test(str)){
		flag = true;
	}
	return flag;
}

/**
 * 显示路径下的文件（夹）
 * @param document 文档对象
 * @param position 当前行与列
 * @param token -
 * @param context 上下文对象
 */
export default function showSrcFile(document:any, position:any, token:any, context:any){
	const line        = document.lineAt(position);
	const projectPath = getProjectSrcPath(document);
	// 只截取到光标位置为止，防止一些特殊情况
	const lineText = line.text.substring(0, position.character);
	let str = lineText.toString().trimRight();
	let flag = checkSrcFile(str);
	if(flag) {
		let filePath = str.slice(str.indexOf('@/')+1);
		let stats = getFileStats(`${projectPath}/src${filePath}`);
		if(!stats.isDirectory()){
			console.log('showSrcFile: isDirty false');
			return ;
		}
		let files = fs.readdirSync(`${projectPath}/src${filePath}`);
		let list = files.map(val => {
			let fileStat = getFileStats(`${projectPath}/src${filePath}/${val}`);
			let pathType : 'Folder' | 'File' = fileStat.isDirectory() ? 'Folder' :'File';
			let item = new vscode.CompletionItem(val, vscode.CompletionItemKind[pathType]);
			item.detail = val;
			if(/\.(tsx|ts|jsx)$/g.test(val)){
				item.label = val.replace(/\.(tsx|ts|jsx)$/g,'');
			}
			return item;
		});
		return new vscode.CompletionList(list);
	}
}