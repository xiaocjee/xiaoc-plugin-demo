import * as vscode from 'vscode';
import * as fs from "fs";
import {getProjectSrcPath,isFileExist} from '../../lib/util';

const scssVariablePath:string = vscode.workspace.getConfiguration().get('xiaoc-vscode.scssVariablePath') || '';

// ==============================  scss变量文件提示 =====================================
/**
 * 提取原子css文件内容，作为className智能提示
 * @param document 文档对象
 * @param position 当前行与列
 * @param token -
 * @param context 上下文对象
 */
export default function showScssVariable(document:any, position:any, token:any, context:any) {
	const projectPath = getProjectSrcPath(document);
	let pathList = scssVariablePath.split(',').filter(item=> isFileExist(`${projectPath}${item}`));
	if(pathList.length===0){
		return ;
	}
	let data = '';
	pathList.forEach(item => {
		data += fs.readFileSync(`${projectPath}${item}`).toString('utf8'); 
	});
	const dependencies = data.match(/\$.*\:/g) || [];
	const list = dependencies.filter((item,index)=>{
		return dependencies.indexOf(item,0) === index;
	}).map(dep => {
		// vscode.CompletionItemKind 表示提示的类型
		let val = dep.slice(0,-1).trim();
		return new vscode.CompletionItem(val, vscode.CompletionItemKind.Field);
	});
	return list;
}