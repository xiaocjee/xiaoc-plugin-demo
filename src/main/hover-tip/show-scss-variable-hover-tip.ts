import * as vscode from 'vscode';
import * as fs from "fs";
import { getProjectSrcPath,isFileExist } from '../../lib/util';
const scssVariablePath:string = vscode.workspace.getConfiguration().get('xiaoc-vscode.scssVariablePath') || '';

// ==============================  scss变量文件提示 =====================================

/**
 * 提示scss全局变量
 * @param document 文档对象
 * @param position 当前行与列
 * @param token -
 */
export default function showScssVariableHoverTip(document:vscode.TextDocument, position:vscode.Position, token:vscode.CancellationToken) {
	const word = document.getText(document.getWordRangeAtPosition(position));
	const projectPath = getProjectSrcPath(document);
	let pathList = scssVariablePath.split(',').filter(item => isFileExist(`${projectPath}${item}`));
	
	if(pathList.length === 0 || word.indexOf("$") === -1 ){
		return ;
	}
	let data = '';
	pathList.forEach(item => {
		data +=  fs.readFileSync(`${projectPath}${item}`).toString('utf8').trim() + '\r\n';
	});

	if(data.indexOf(word) !== -1){
		let list = data.split(/\r\n/g).filter(item => item && (item.indexOf("//") > 3 || item.indexOf('//') === -1)).map(item => item.replace(/(\s)|(\t)/g,''));
		let lineList = list.filter(item => item.indexOf(word) !== -1 ).map(item=>item.split(/(\:)|(\;)|(\/\/)/g).filter(item1=> item1 && item1 !==':'&& item1 !==';' && item1 !=='//'));
		let content = data;
		if(lineList.length > 0){
			let item = lineList[0];
			if(/^#([0-9][a-f][A-F]){6}$/.test(item[1])){
				content = `${item[2] ? '* ' + item[2] : ''}\n* ${item[0]} : $\color{${item[1]}}{${item[1]}}$`;
			}else{
				content = `${item[2] ? '* ' + item[2] : ''}\n* ${item[0]} : **${item[1]}**`;
			}
		}
		// hover内容支持markdown语法
		return new vscode.Hover(content);
	}
}
