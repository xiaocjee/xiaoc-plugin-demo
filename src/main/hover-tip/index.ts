import * as vscode from 'vscode';

import showScssVariableHoverTip from './show-scss-variable-hover-tip';

module.exports = function(context:vscode.ExtensionContext) {
	// 注册鼠标悬停提示
	context.subscriptions.push(vscode.languages.registerHoverProvider('scss', {
		provideHover:showScssVariableHoverTip
	}));
};
