import * as fs from 'fs';
import * as vscode from 'vscode';
import * as path from 'path';
import * as os from 'os';
import {exec} from 'child_process';

export function getProjectSrcPath(document:any) {
	if (!document) {
			document = vscode.window.activeTextEditor ? vscode.window.activeTextEditor.document : null;
	}
	if (!document) {
			showError('当前激活的编辑器不是文件或者没有文件被打开！');
			return '';
	}
	const currentFile = document.uri.path;
	let projectPath = '';

	let workspaceFolders = vscode.workspace.workspaceFolders ? vscode.workspace.workspaceFolders.map(item => item.uri.path) : [];
	// 由于存在Multi-root工作区，暂时没有特别好的判断方法，先这样粗暴判断
	// 如果发现只有一个根文件夹，读取其子文件夹作为 workspaceFolders

	if(workspaceFolders.length>1){
		for (const item of workspaceFolders) {
			if(currentFile.indexOf(item) !== -1){
				projectPath = item;
				break ;
			}
		}
	}else {
		projectPath = workspaceFolders[0];
	}

	if (!projectPath) {
			showError('获取工程根路径异常！');
			return '';
	}
	console.log('projectPath', projectPath);
	return projectPath.slice(1,2).toUpperCase() + projectPath.slice(2);
}

/**
 * 弹出错误信息
 */
export function showError(info:any) {
		vscode.window.showErrorMessage(info);
}

/**
 * 弹出提示信息
 */
export function showInfo(info:any) {
		vscode.window.showInformationMessage(info);
}

export function getFileStats(path:any){
	return fs.statSync(path);
}

export function isFileExist(path:any) {
	try{
		fs.accessSync(path,fs.constants.R_OK);
	}catch(e){
		console.log('isFileExist error',e);
		return false;
	}
	return true;
}

/**
 * 获取某个扩展文件绝对路径
 * @param context 上下文
 * @param relativePath 扩展中某个文件相对于根目录的路径，如 images/test.jpg
 */
export function getExtensionFileAbsolutePath(context:any, relativePath:any) {
	return path.join(context.extensionPath, relativePath);
}


/**
 * 获取某个扩展文件相对于webview需要的一种特殊路径格式
 * 形如：vscode-resource:/Users/toonces/projects/vscode-cat-coding/media/cat.gif
 * @param context 上下文
 * @param relativePath 扩展中某个文件相对于根目录的路径，如 images/test.jpg
 */
export function getExtensionFileVscodeResource(context:any, relativePath:any) {
	const diskPath = vscode.Uri.file(path.join(context.extensionPath, relativePath));
	return diskPath.with({ scheme: 'vscode-resource' }).toString();
}


/**
 * 在Finder中打开某个文件或者路径
 */
export function openFileInFinder(filePath:any) {
	if (!fs.existsSync(filePath)) {
			showError('文件不存在：' + filePath);
	}
	// 如果是目录，直接打开就好
	if (fs.statSync(filePath).isDirectory()) {
			exec(`open ${filePath}`);
	} else {
			// 如果是文件，要分开处理
			const fileName = path.basename(filePath);
			filePath = path.dirname(filePath);
			// 这里有待完善，还不知道如何finder中如何选中文件
			exec(`open ${filePath}`);
	}
}

/**
 * 获取当前工程名
 */
export function getProjectName(projectPath:any) {
	return path.basename(projectPath);
}

/**
 * 在vscode中打开某个文件
 * @param {*} path 文件绝对路径
 * @param {*} text 可选，如果不为空，则选中第一处匹配的对应文字
 */
export function openFileInVscode(path:any, text:any) {
	let options = {};
	if (text) {
			const selection = getStrRangeInFile(path, text);
			options = { selection };
	}
	vscode.window.showTextDocument(vscode.Uri.file(path), options);
}

/**
 * 获取某个字符串在文件里第一次出现位置的范围，
 */
export function getStrRangeInFile(filePath:any, str:any) {
	var pos = findStrInFile(filePath, str);
	return new vscode.Range(new vscode.Position(pos.row, pos.col), new vscode.Position(pos.row, pos.col + str.length));
}

/**
 * 从某个文件里面查找某个字符串，返回第一个匹配处的行与列，未找到返回第一行第一列
 * @param filePath 要查找的文件
 * @param reg 正则对象，最好不要带g，也可以是字符串
 */
export function findStrInFile(filePath:any, reg:any) {
	const content = fs.readFileSync(filePath, 'utf-8');
	reg = typeof reg === 'string' ? new RegExp(reg, 'm') : reg;
	// 没找到直接返回
	if (content.search(reg) < 0) return {row: 0, col: 0};
	const rows = content.split(os.EOL);
	// 分行查找只为了拿到行
	for(let i = 0; i < rows.length; i++) {
			let col = rows[i].search(reg);
			if(col >= 0) {
					return {row: i, col};
			}
	}
	return {row: 0, col: 0};
}

/**
 * 使用默认浏览器中打开某个URL
 */
export function openUrlInBrowser(url:any) {
	exec(`open '${url}'`);
}

/**
 * 获取当前所在工程根目录，有3种使用方法：<br>
 * getProjectPath(uri) uri 表示工程内某个文件的路径<br>
 * getProjectPath(document) document 表示当前被打开的文件document对象<br>
 * getProjectPath() 会自动从 activeTextEditor 拿document对象，如果没有拿到则报错
 * @param {*} document
 */
export function getProjectPath(uri:any) {
	if (!uri.path) {
			showError('当前激活的编辑器不是文件或者没有文件被打开！');
			return '';
	}
	const currentFile = uri.path;
	let projectPath = '';

	let workspaceFolders = vscode.workspace.workspaceFolders ? vscode.workspace.workspaceFolders.map(item => item.uri.path) : [];
	// 由于存在Multi-root工作区，暂时没有特别好的判断方法，先这样粗暴判断
	// 如果发现只有一个根文件夹，读取其子文件夹作为 workspaceFolders

	if(workspaceFolders.length>1){
		for (const item of workspaceFolders) {
			if(currentFile.indexOf(item) !== -1){
				projectPath = item;
				break ;
			}
		}
	}else {
		projectPath = workspaceFolders[0];
	}

	if (!projectPath) {
			showError('获取工程根路径异常！');
			return '';
	}
	return projectPath;
}

/**
 * 从某个HTML文件读取能被Webview加载的HTML内容
 * @param {*} context 上下文
 * @param {*} templatePath 相对于插件根目录的html文件相对路径
 */
export function getWebViewContent(context:vscode.ExtensionContext, templatePath:any) {
  const resourcePath = getExtensionFileAbsolutePath(context, templatePath);
  const dirPath = path.dirname(resourcePath);
  let html = fs.readFileSync(resourcePath, 'utf-8');
  // vscode不支持直接加载本地资源，需要替换成其专有路径格式，这里只是简单的将样式和JS的路径替换
  html = html.replace(/(<link.+?href="|<script.+?src="|<img.+?src=")(.+?)"/g, (m, $1, $2) => {
      return $1 + vscode.Uri.file(path.resolve(dirPath, $2)).with({ scheme: 'vscode-resource' }).toString() + '"';
  });
  return html;
}

/**
 * 执行回调函数
 * @param {*} panel
 * @param {*} message
 * @param {*} resp
 */
export function invokeCallback(panel:any, message:any, resp:any) {
	console.log('回调消息：', resp);
	// 错误码在400-600之间的，默认弹出错误提示
	if (typeof resp == 'object' && resp.code && resp.code >= 400 && resp.code < 600) {
			showError(resp.message || '发生未知错误！');
	}
	panel.webview.postMessage({cmd: 'vscodeCallback', cbid: message.cbid, data: resp});
}
